import { IOption } from "../interfaces/IOption";

export class Option implements IOption{
    optionId:number;
    optionDescription?:string;
    active?:boolean;
    class?:string;
    createdUserCode?:string;
    createdDateTime?:Date;
    updatedUserCode?:string;
    updatedDateTime?:Date;
    state?:boolean;
    constructor(){
        this.active = false
        this.optionId = 0
    }
}
