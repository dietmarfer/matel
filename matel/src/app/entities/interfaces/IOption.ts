export interface IOption {
    optionId:number;
    optionDescription?:string;
    active?:boolean;
    class?:string;
    createdUserCode?:string;
    createdDateTime?:Date;
    updatedUserCode?:string;
    updatedDateTime?:Date;
    state?:boolean;
}
  