import { Component, OnInit } from '@angular/core';
import { LocationComponent } from './location/location.component';
import { Option } from '../entities/classes/Option';
import { IOption } from '../entities/interfaces/IOption';
@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {
  status = false;
  Title = "";
  Options:IOption[] = [];
  constructor() { }

  ngOnInit(): void {
    this.CreateOptions();
    this.SetPage(this.Options[0]);
  }

  addToggle()
  {
    this.status = !this.status;       
  }

  SetPage(option:Option){
    this.Options = this.Options.map(option => {
        return { ...option, active: false };
    });
    let foundOption = this.Options.find(x=>x.optionId == option.optionId);
    if(foundOption != null){
      foundOption.active = true;
    }
    this.Title = option?.optionDescription ?? "";
  }

  CreateOptions(){
    this.Options = [
      { 
        optionId: 1,
        optionDescription: 'Principal',
        active: true,
        class:'bx bxs-home pe-auto',
      },
      { 
        optionId: 2,
        optionDescription: 'Personal',
        active: false,
        class:'bx bxs-user pe-auto',
      },
      { 
        optionId: 3,
        optionDescription: 'Ubicaciones',
        active: false,
        class:'bx bxs-map-pin pe-auto',
      },
      { 
        optionId: 4,
        optionDescription: 'Programaciones',
        active: false,
        class:'bx bxs-calendar pe-auto',
      },
      { 
        optionId: 5,
        optionDescription: 'Cargos',
        active: false,
        class:'bx bxs-calendar-check pe-auto',
      }

    ]
  }

}
